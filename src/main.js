import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueCurrencyFilter from 'vue-currency-filter'

Vue.config.productionTip = false

Vue.use(VueCurrencyFilter,
{
  symbol : '$',
  thousandsSeparator: ',',
  fractionCount: 2,
  fractionSeparator: '.',
  symbolPosition: 'front',
  symbolSpacing: true
})

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
